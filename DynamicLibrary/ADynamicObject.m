//
//  ADynamicObject.m
//  DynamicLibrary
//
//  Created by fadi on 3/10/19.
//  Copyright © 2019 Experiments. All rights reserved.
//

#import "ADynamicObject.h"

@implementation ADynamicObject

+(void)load {
    [super load];
    NSLog(@"The dynamic class is loaded");
}

-(int)somethingWithParameter:(int) parameter andNumber:(int) number {
    NSLog(@"Numbers are being multiplied, %i, %i", parameter, number);
    return parameter * number;
}

@end
